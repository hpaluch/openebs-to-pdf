// export OpenEBS Docusaurus docs to PDF
// based on: https://github.com/facebook/docusaurus/issues/969#issuecomment-595012118
const puppeteer = require('puppeteer-core');
const { PDFRStreamForBuffer, createWriterToModify, PDFStreamForResponse } = require('hummus');
const { WritableStream } = require('memory-streams');
const fs = require('fs');

const mergePdfBlobs = (pdfBlobs) => {
  const outStream = new WritableStream();                                                                                                                                             
  const [firstPdfRStream, ...restPdfRStreams] = pdfBlobs.map(pdfBlob => new PDFRStreamForBuffer(pdfBlob));
  const pdfWriter = createWriterToModify(firstPdfRStream, new PDFStreamForResponse(outStream));

  restPdfRStreams.forEach(pdfRStream => pdfWriter.appendPDFPagesFromPDF(pdfRStream));

  pdfWriter.end();
  outStream.end();
  
  return outStream.toBuffer();
};


let generatedPdfBlobs = [];

(async () => {
  const browser = await puppeteer.launch({ executablePath: '/usr/bin/chromium' });
  let page = await browser.newPage();
  let nextPageUrl = 'http://localhost:3000/docs/';

  while (nextPageUrl) {
	  console.log(`Crawling: '${nextPageUrl}'`);
    await page.goto(`${nextPageUrl}`, {waitUntil: 'networkidle2'});
      
    try {
      nextPageUrl = await page.$eval('.pagination-nav__item--next > a', (element) => {
        return element.href;
      });
    } catch (e) {
      nextPageUrl = null;
    }
  
  
    let html = await page.$eval('article', (element) => {
      return element.outerHTML;
    });
  
    await page.setContent(html);
    // remove not wanted content
    // from: https://stackoverflow.com/a/51488147
    await page.evaluate(() => {
     // remove Search bar
     let toDel = document.querySelector('div.searhBar');
         toDel.parentNode.removeChild(toDel);
     // remove Edit page button
     let toDel2 = document.querySelector('div.actionButtons_src-theme-DocItem-styles-module');
         toDel2.parentNode.removeChild(toDel2);
    });

    await page.addStyleTag({url: 'http://localhost:3000/docs/styles.css'});
    //await page.addScriptTag({url: 'http://localhost:3000/styles.js'});
    const pdfBlob = await page.pdf({path: "", format: 'A4', printBackground: true, margin : {top: 20, right: 15, left: 15, bottom: 20}});

    generatedPdfBlobs.push(pdfBlob);
  }
  await browser.close();

  const mergedPdfBlob = mergePdfBlobs(generatedPdfBlobs);
  fs.writeFileSync('openebs-docs.pdf', mergedPdfBlob);
})();
