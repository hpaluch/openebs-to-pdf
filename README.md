# Attempt to export OpenEBS website to PDF

Small helper to export OpenEBS Documentation to PDF.

Based on:
- https://github.com/facebook/docusaurus/issues/969#issuecomment-595012118


## Setup

Tested on: `openSUSE Tumbleweed 20211102`

Install following packages:
```bash
sudo zypper in npm chromium make gcc gcc-c++ nodejs-devel git-core
```

To prepare OpenEBS docs for export (Docusaurus) do this:
- checkout OpenEBS website project:
  ```bash
  mkdir -p ~/projects
  cd ~/projects
  git clone https://github.com/openebs/website.git openebs-website
  cd openebs-website/docs/
  ```
- this version was testesd:
  ```bash
  $ git describe --always --long --dirty

  27420bc-dirty
  ```
- now install project packages using:
  ```
  npm i
  ```
- now run Docusaurus server:
  ```bash
  npm start
  ```
- verify that site is available on:
  - http://localhost:3000/docs


Now prepare and run this project to export above site to PDF:
- prepare project:
  ```bash
  cd ~/projects
  git clone https://gitlab.com/hpaluch/openebs-to-pdf.git
  cd ../openebs-to-pdf 
  npm -i
  ```
- to export website to PDF run and wait:
  ```bash
  npm run start
  ```
- on finish there should be new PDF file:
  - `openebs-docs.pdf`


# Resources

* https://openebs.io/docs/
* https://github.com/puppeteer/puppeteer
* https://github.com/facebook/docusaurus/issues/969#issuecomment-595012118

--hp

